<?php

session_start();

require 'vendor/autoload.php';

function autoload($class)
{
	// for namespace
	$explodedClass = explode('\\', $class);
	$class         = end($explodedClass);
	$prefix        = '';

	$folders = array(
		'lib',
		'lib/helper',
		'lib/db/entity',
		'lib/db/manager'
	);

	foreach ($folders as $folder) {
		if (is_file($prefix . $folder . '/' . $class . '.php')) {
			include_once $prefix . $folder . '/' . $class . '.php';
		}
	}
}

spl_autoload_register('autoload');