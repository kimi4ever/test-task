$(document).ready(function () {
	Product.init();
});

/**
 * The frontend functionality of the product/basket page.
 *
 * @type {{init: Product.init, onProductMenuClick: Product.onProductMenuClick, onBasketMenuClick: Product.onBasketMenuClick, onAddToCartClick: Product.onAddToCartClick, onRemoveFromCartClick: Product.onRemoveFromCartClick, loadProducts: Product.loadProducts, loadBasket: Product.loadBasket, fillBasket: Product.fillBasket}}
 */
var Product = {
	/**
	 * Load the page content (products/basket) and initialize the javascript events.
	 */
	init: function () {
		this.loadProducts();
		this.loadBasket();

		$('#product-menu').click(this.onProductMenuClick);
		$('#basket-menu').click(this.onBasketMenuClick);
		$('#empty-cart').click(this.onRemoveFromCartClick);
		$('#product-sort-select').on('change', this.loadProducts);
		$(document).on('click', '.add-to-cart', this.onAddToCartClick);
		$(document).on('click', '.remove-from-cart', this.onRemoveFromCartClick);
	},

	/**
	 * Event
	 * Calls when user clicks on product menu item.
	 *
	 * @param e
	 */
	onProductMenuClick: function (e) {
		e.preventDefault();

		$('#basket-container').addClass('d-none');
		$('#product-container').removeClass('d-none');
	},

	/**
	 * Event
	 * Calls when user clicks on cart menu item.
	 *
	 * @param e
	 */
	onBasketMenuClick: function (e) {
		e.preventDefault();

		$('#product-container').addClass('d-none');
		$('#basket-container').removeClass('d-none');
	},

	/**
	 * Event
	 * Calls when user clicks on one of the add to cart buttons.
	 *
	 * @param e
	 */
	onAddToCartClick: function (e) {
		e.preventDefault();

		var id = $(this).data('id');

		$.ajax({
			url:      '/?api=basket&id=' + id,
			method:   'post',
			dataType: 'json',
			success:  function (response) {
				Product.fillBasket(response);
			}
		});
	},

	/**
	 * Event
	 * Calls when user clicks on one of the remove from cart buttons.
	 * @param e
	 */
	onRemoveFromCartClick: function (e) {
		e.preventDefault();

		var id  = $(this).data('id'),
			url = '/?api=basket' + (typeof id !== 'undefined' ? ('&id=' + id) : '');

		$.ajax({
			url:      url,
			method:   'delete',
			dataType: 'json',
			success:  function (response) {
				Product.fillBasket(response);
			}
		});
	},

	/**
	 * Load the products.
	 */
	loadProducts: function () {
		$.ajax({
			url:      '/?api=product&order=' + $('#product-sort-select').val(),
			dataType: 'json',
			success:  function (response) {
				var container = $('#product-container').find('.products');

				container.html('');

				$.each(response, function (i, v) {
					container.append(
						'<div class="card">' +
						'   <div class="card-body">' +
						'       <h4 class="card-title">' + v.title + ' <small class="text-muted">(' + v.author + ')</small></h4>' +
						'       <div><small class="text-muted">-' + v.id + '-</small></div>' +
						'       <div class="text-muted">' +
						'           Published by ' + v.publisher +
						'       </div>' +
						'       <div class="row">' +
						'           <div class="col-sm-6">' +
						'               <h5>' + (v.type ? '<small class="text-muted" style="text-decoration: line-through;">' + v.price + ' Ft</small> ' + v.calculated : v.price) + ' Ft</h5>' +
						'           </div>' +
						'           <div class="col-sm-6 text-right">' +
						'               <a href="#" class="add-to-cart btn btn-primary" data-id="' + v.id + '">Add to Cart</a>' +
						'           </div>' +
						'       </div>' +
						'   </div>' +
						'</div>'
					);
				});
			}
		});
	},

	/**
	 * Load the basket.
	 */
	loadBasket: function () {
		$.ajax({
			url:      '/?api=basket',
			dataType: 'json',
			success:  function (response) {
				Product.fillBasket(response);
			}
		});
	},

	/**
	 * Fill the small basket and the basket menu with the correct data.
	 *
	 * @param products
	 */
	fillBasket: function (products) {
		var container     = $('.small-basket'),
			cartContainer = $('#basket-container').find('.products'),
			price         = products.price,
			present       = products.present,
			full          = products.full,
			discount      = products.discount;

		delete products.present;
		delete products.price;
		delete products.full;
		delete products.discount;
		container.html('');
		cartContainer.html('');

		$.each(products, function (i, v) {
			container.append(
				'<div class="card">' +
				'   <div class="card-body">' +
				'       <h5>' + v.title + ' <small class="text-muted">(' + v.quantity + ')</small></h5>' +
				'       <div class="text-muted">' + v.qPrice + ' Ft</div>' +
				'       <a href="#" class="btn btn-danger remove-from-cart" data-id="' + v.id + '">Remove</a>' +
				'   </div>' +
				'</div>'
			);

			cartContainer.append(
				'<div class="card">' +
				'   <div class="card-body">' +
				'       <h4 class="card-title">' + v.title + ' <small class="text-muted">(' + v.author + ')</small> <a href="#" class="btn btn-danger remove-from-cart float-right" data-id="' + v.id + '">Remove</a></h4>' +
				'       <div><small class="text-muted">-' + v.id + '-</small></div>' +
				'       <div class="text-muted">' +
				'           Published by ' + v.publisher +
				'       </div>' +
				'       <div class="row">' +
				'           <div class="col-sm-6">' +
				'               <h5>' + (v.type ? '<small class="text-muted" style="text-decoration: line-through;">' + v.price + ' Ft</small> ' + v.calculated : v.price) + ' Ft -- Quantity: ' + v.quantity + ' -- Sum: ' + v.qPrice + ' Ft</h5>' +
				'           </div>' +
				'       </div>' +
				'   </div>' +
				'</div>'
			);
		});

		container.append(price + ' Ft');

		container.append(
			'<div class="text-muted">' +
			'   ' + (present > 0 ? '<div class="text-muted">You acquire ' + present + ' piece of book' + (present > 1 ? 's' : '') + ' by free gift!' : '') +
			'</div>'
		);

		cartContainer.append(
			'<div class="text-right">' +
			'   <h4><small class="text-muted">Sum without discount: </small> ' + full + ' Ft</h4>' +
			'   <h4><small class="text-muted">Discount: </small> ' + discount + ' Ft</h4>' +
			'   <h4><small class="text-muted">Sum: </small> ' + price + ' Ft</h4>' +
			'</div>'
		);
	}
};