-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Gép: localhost:3306
-- Létrehozás ideje: 2017. Okt 30. 19:16
-- Kiszolgáló verziója: 5.6.28
-- PHP verzió: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Adatbázis: `bigfish`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `discount`
--

CREATE TABLE `discount` (
  `id` int(11) NOT NULL,
  `type` text COLLATE utf8_unicode_ci,
  `productId` int(11) DEFAULT NULL,
  `publisherName` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `discount`
--

INSERT INTO `discount` (`id`, `type`, `productId`, `publisherName`) VALUES
(101, '* 0.9', 1006, ''),
(102, '- 500', 1002, ''),
(103, '2+1', 0, 'PANEM');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci,
  `author` text COLLATE utf8_unicode_ci,
  `publisher` text COLLATE utf8_unicode_ci,
  `price` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `product`
--

INSERT INTO `product` (`id`, `title`, `author`, `publisher`, `price`) VALUES
(1001, 'Dreamweaver CS4', 'Janine Warner', 'PANEM', 3900),
(1002, 'JavaScript kliens oldalon', 'Sikos László', 'BBS-INFO', 2900),
(1003, 'Java', 'Barry Burd', 'PANEM', 3700),
(1004, 'C# 2008', 'Stephen Randy Davis', 'PANEM', 3700),
(1005, 'Az Ajax alapjai', 'Joshua Eichorn', 'PANEM', 4500),
(1006, 'Algoritmusok', 'Ivanyos Gábor, Rónyai Lajos, Szabó Réka', 'TYPOTEX', 3600);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `discount`
--
ALTER TABLE `discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT a táblához `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1007;