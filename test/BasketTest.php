<?php
/**
 * Package test
 * Holds the BasketTest class
 */

include_once __DIR__ . '/../boot.php';

use PHPUnit\Framework\TestCase;

/**
 * Class BasketTest
 *
 * Test the functionality of the BasketManager class.
 */
class BasketTest extends TestCase
{
	/**
	 * Test a simple post to the basket with simple discount.
	 * Test the get, post methods.
	 */
	public function testSingleSimple()
	{
		$manager = new BasketManager();
		$manager->setId(1006);
		$result = $manager->post();

		$this->assertEquals(0, $result['present']);
		$this->assertEquals(3240, $result['price']);
		$this->assertEquals(360, $result['discount']);
		$this->assertEquals(3600, $result['full']);
	}

	/**
	 * Test a double post to the basket with simple discount.
	 * Test the get, post methods.
	 */
	public function testDoubleSimple()
	{
		$manager = new BasketManager();
		$manager->setId(1006);
		$manager->post();
		$manager->setId(1002);
		$result = $manager->post();

		$this->assertEquals(0, $result['present']);
		$this->assertEquals(5640, $result['price']);
		$this->assertEquals(860, $result['discount']);
		$this->assertEquals(6500, $result['full']);
	}

	/**
	 * Test the 2+1 discount mixed with the simple discounts.
	 * Test the get, put, post, delete methods.
	 */
	public function testTwoPlusOne()
	{
		$manager = new BasketManager();
		$manager->setId(1005);
		$result = $manager->put();

		$this->assertEquals(0, $result['present']);
		$this->assertEquals(4500, $result['price']);
		$this->assertEquals(0, $result['discount']);
		$this->assertEquals(4500, $result['full']);

		$manager->setId(1004);
		$result = $manager->post();

		$this->assertEquals(0, $result['present']);
		$this->assertEquals(8200, $result['price']);
		$this->assertEquals(0, $result['discount']);
		$this->assertEquals(8200, $result['full']);

		$manager->setId(1002);
		$result = $manager->post();

		$this->assertEquals(0, $result['present']);
		$this->assertEquals(10600, $result['price']);
		$this->assertEquals(500, $result['discount']);
		$this->assertEquals(11100, $result['full']);

		$manager->setId(1004);
		$result = $manager->post();

		$this->assertEquals(1, $result['present']);
		$this->assertEquals(10600, $result['price']);
		$this->assertEquals(4200, $result['discount']);
		$this->assertEquals(14800, $result['full']);

		$result = $manager->delete();

		$this->assertEquals(0, $result['present']);
		$this->assertEquals(6900, $result['price']);
		$this->assertEquals(500, $result['discount']);
		$this->assertEquals(7400, $result['full']);

		$manager->post();
		$manager->post();
		$manager->post();
		$manager->post();
		$result = $manager->post();

		$this->assertEquals(2, $result['present']);
		$this->assertEquals(18000, $result['price']);
		$this->assertEquals(7900, $result['discount']);
		$this->assertEquals(25900, $result['full']);

		$manager->setId(null);
		$result = $manager->delete();

		$this->assertEquals(0, $result['present']);
		$this->assertEquals(0, $result['price']);
		$this->assertEquals(0, $result['discount']);
		$this->assertEquals(0, $result['full']);
	}
}