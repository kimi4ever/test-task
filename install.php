<?php

include_once 'boot.php';

$entities = glob('lib/db/entity/*.php');
$creator  = new CreatorManager();

foreach ($entities as $entity) {
	$creator->createTable($entity);
}

$creator->installPredefinedData();