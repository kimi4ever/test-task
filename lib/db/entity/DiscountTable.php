<?php
/**
 * Package lib/db/entity
 * Holds the DiscountTable class
 */

/**
 * Class DiscountTable
 *
 * Stores the fields of the discount table.
 * This solution improves the speed of the development if the programmer uses an IDE.
 */
class DiscountTable
{
	/** Discount table name string */
	const NAME = 'discount';

	/**
	 * Numeric identifier of the discount.
	 *
	 * @mysql int auto_increment
	 */
	const FIELD_ID              = 'id';
	/**
	 * Type of the discount. If the discount can be represented by a simple mathematical expression (ex. + 500),
	 * then this expression will be stored in that field, otherwise the field stores a key to the type of the discount
	 * (ex. 2+1).
	 *
	 * @mysql text
	 */
	const FIELD_TYPE            = 'type';
	/**
	 * Identifier of the discounted product.
	 *
	 * @mysql int
	 */
	const FIELD_PRODUCT_ID      = 'productId';
	/**
	 * Name of the discounted publisher.
	 * The comparison based on the product's publisher field. (String to string comparison)
	 *
	 * @mysql text
	 */
	const FIELD_PUBLISHER_NAME  = 'publisherName';
}