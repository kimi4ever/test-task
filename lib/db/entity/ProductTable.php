<?php
/**
 * Package lib/db/entity
 * Holds the ProductTable class
 */

/**
 * Class ProductTable
 *
 * Stores the fields of the product table.
 * This solution improves the speed of the development if the programmer uses an IDE.
 */
class ProductTable
{
	/** Product table name string. */
	const NAME = 'product';

	/**
	 * Numeric identifier of the product.
	 *
	 * @mysql int auto_increment
	 */
	const FIELD_ID        = 'id';
	/**
	 * The title of the product.
	 *
	 * @mysql text
	 */
	const FIELD_TITLE     = 'title';
	/**
	 * The author of the product.
	 *
	 * @mysql text
	 */
	const FIELD_AUTHOR    = 'author';
	/**
	 * The publisher of the product.
	 *
	 * @mysql text
	 */
	const FIELD_PUBLISHER = 'publisher';
	/**
	 * Price of the product.
	 *
	 * @mysql int
	 */
	const FIELD_PRICE     = 'price';
}