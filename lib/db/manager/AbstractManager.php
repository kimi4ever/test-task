<?php
/**
 * Package lib/db/manager
 * Holds the AbstractManager class
 */

use Medoo\Medoo;

/**
 * Class AbstractManager
 *
 * Initialize the database connection on construction.
 */
abstract class AbstractManager
{
	/**
	 * Hold the database connection.
	 *
	 * @var Medoo
	 */
	protected $database;

	/**
	 * AbstractManager constructor.
	 *
	 * Initialize the database connection.
	 */
	public function __construct()
	{
		$config         = require(__DIR__ . '/../config.php');
		$this->database = new Medoo($config);
	}
}