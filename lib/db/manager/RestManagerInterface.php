<?php
/**
 * Package lib/db/manager
 * Holds the RestManagerInterface interface
 */

/**
 * Interface RestManagerInterface
 *
 * Summary of what a manager have to implement.
 */
interface RestManagerInterface
{
	/** Rest get method */
	public function get();
	/** Rest post method */
	public function post();
	/** Rest put method */
	public function put();
	/** Rest delete method */
	public function delete();
	/**
	 * Set the order of the rest call.
	 *
	 * @param $order
	 */
	public function setOrder($order);
	/**
	 * Set the id of the rest call.
	 *
	 * @param $id
	 */
	public function setId($id);
	/**
	 * Set plus parameters to the rest call.
	 *
	 * @param $data
	 */
	public function setData($data);
}