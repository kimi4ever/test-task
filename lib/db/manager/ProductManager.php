<?php
/**
 * Package lib/db/manager
 * Holds the ProductManager class
 */

/**
 * Class ProductManager
 *
 * Manage the products stored in the database.
 */
class ProductManager extends AbstractManager implements RestManagerInterface
{
	/**
	 * Order type.
	 *
	 * @var string
	 */
	protected $order = 'name';
	/**
	 * Numeric identifier of the product work with.
	 *
	 * @var int
	 */
	protected $id;
	/**
	 * Multiple identifiers of the products.
	 *
	 * @var array
	 */
	protected $identifiers;
	/**
	 * More information of the product.
	 *
	 * @var array
	 */
	protected $data;

	/**
	 * Gives back the products stored in the database.
	 *
	 * @return array
	 */
	public function get()
	{
		$where = array(
			'ORDER' => ($this->order == 'name') ? ProductTable::FIELD_TITLE : ProductTable::FIELD_PRICE
		);

		if (!empty($this->identifiers)) {
			$where[ProductTable::NAME . '.' . ProductTable::FIELD_ID] = $this->identifiers;
		}

		$result = $this->database->select(
			ProductTable::NAME,
			array(
				'[>]' . DiscountTable::NAME => array(ProductTable::FIELD_ID => DiscountTable::FIELD_PRODUCT_ID)
			),
			array(
				ProductTable::NAME . '.' . ProductTable::FIELD_ID,
				ProductTable::NAME . '.' . ProductTable::FIELD_TITLE,
				ProductTable::NAME . '.' . ProductTable::FIELD_AUTHOR,
				ProductTable::NAME . '.' . ProductTable::FIELD_PUBLISHER,
				ProductTable::NAME . '.' . ProductTable::FIELD_PRICE,
				DiscountTable::NAME . '.' . DiscountTable::FIELD_TYPE
			),
			$where
		);

		foreach ($result as $key => $item) {
			$result[$key]['calculated'] = !empty($item[DiscountTable::FIELD_TYPE]) && $item[DiscountTable::FIELD_TYPE] != '2+1'
				? eval('return ' . $item[ProductTable::FIELD_PRICE] . $item[DiscountTable::FIELD_TYPE] . ';')
				: $item[ProductTable::FIELD_PRICE];
		}

		if ($this->order == 'price') {
			usort($result, function ($a, $b) {if ($a['calculated'] == $b['calculated']) return 0; return ($a['calculated'] < $b['calculated']) ? -1 : 1;});
		}

		return $result;
	}

	/**
	 * Updates a product with the given data according to the given identifier.
	 */
	public function post()
	{
		if (!empty($this->id) && !empty($this->data)) {
			$this->database->update(ProductTable::NAME, $this->data, array(ProductTable::FIELD_ID => $this->id));
		}
	}

	/**
	 * Puts new product to the database.
	 *
	 * @return bool|int   The last inserted row's identifier.
	 */
	public function put()
	{
		if (!empty($this->data)) {
			$this->database->insert(ProductTable::NAME, $this->data);
			return $this->database->id();
		}

		return false;
	}

	/**
	 * Deletes product with the given identifier.
	 */
	public function delete()
	{
		if (!empty($this->id)) {
			$this->database->delete(ProductTable::NAME, array(ProductTable::FIELD_ID => $this->id));
		}
	}

	/**
	 * Set the order of the products.
	 *
	 * @param $order
	 */
	public function setOrder($order)
	{
		$this->order = $order;
	}

	/**
	 * Set the identifier of the product work with.
	 *
	 * @param $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * Set the identifiers of the products work with.
	 *
	 * @param array $identifiers
	 */
	public function setIdentifiers($identifiers)
	{
		$this->identifiers = $identifiers;
	}

	/**
	 * More information of the product work with.
	 *
	 * @param array $data
	 */
	public function setData($data)
	{
		$this->data = $data;
	}
}