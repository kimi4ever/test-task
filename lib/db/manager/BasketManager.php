<?php
/**
 * Package lib/db/manager
 * Holds the BasketManager class
 */

/**
 * Class BasketManager
 *
 * Stores the basket data in the global SESSION variable.
 */
class BasketManager extends AbstractManager implements RestManagerInterface
{
	/**
	 * Stores the order type of the basket.
	 * This functionality is not implemented yet.
	 *
	 * @var string
	 */
	protected $order;
	/**
	 * Stores the actual identifier of the product working with.
	 *
	 * @var int
	 */
	protected $id;
	/**
	 * Stores the
	 *
	 * @var array
	 */
	protected $data;

	/**
	 * Prepare the product array put into the basket.
	 * The function gives back the products stored in the basket, how many present product the user has, the price of
	 * the products with and without the discount and the current discount.
	 *
	 * @return array
	 */
	public function get()
	{
		$result               = array();
		$price                = 0;
		$priceWithoutDiscount = 0;
		$presentNum           = 0;

		if (!empty($_SESSION['basket'])) {
			$publisherDiscount = $this->database->select(DiscountTable::NAME, array(DiscountTable::FIELD_PUBLISHER_NAME), array(DiscountTable::FIELD_TYPE => '2+1'));
			$publisherDiscount = array_map(function ($v) {return $v[DiscountTable::FIELD_PUBLISHER_NAME];}, $publisherDiscount);
			$twoPlusOne        = array();
			$manager           = new ProductManager();

			$manager->setIdentifiers(array_keys($_SESSION['basket']));

			$result = $manager->get();

			foreach ($result as $key => $item) {
				$quantity                  = $_SESSION['basket'][$item[ProductTable::FIELD_ID]];
				$result[$key]['quantity']  = $quantity;
				$tmpPrice                  = $quantity * $item['calculated'];
				$result[$key]['qPrice']    = $tmpPrice;
				$price                    += $tmpPrice;
				$priceWithoutDiscount     += $quantity * $item[ProductTable::FIELD_PRICE];

				if (in_array($item[ProductTable::FIELD_PUBLISHER], $publisherDiscount)) {
					$twoPlusOne[$item[ProductTable::FIELD_ID]] = array(
						'price'    => $item[ProductTable::FIELD_PRICE],
						'quantity' => $quantity
					);
				}
			}

			$pieceCount = array_sum(array_map(function ($v) {return $v['quantity'];}, $twoPlusOne));
			$presentNum = floor($pieceCount / 3);

			for ($i = 0; $i < $presentNum; ++$i) {
				$min                           = array_search(min($twoPlusOne), $twoPlusOne);
				$price                        -= $twoPlusOne[$min]['price'];
				$twoPlusOne[$min]['quantity'] -= 1;

				if ($twoPlusOne[$min]['quantity'] <= 0) {
					unset($twoPlusOne[$min]);
				}
			}
		}

		$result['present']  = $presentNum;
		$result['price']    = $price;
		$result['discount'] = $priceWithoutDiscount - $price;
		$result['full']     = $priceWithoutDiscount;

		return $result;
	}

	/**
	 * Add new product to the basket or enlarge the quantity of the stored product.
	 *
	 * @return array   The products in the basket.
	 */
	public function post()
	{
		if (!empty($this->id)) {
			$_SESSION['basket'][$this->id] = !empty($_SESSION['basket'][$this->id]) ? $_SESSION['basket'][$this->id] + 1 : 1;
		}

		return $this->get();
	}

	/**
	 * Add new product to the basket.
	 *
	 * @return array   The products in the basket.
	 */
	public function put()
	{
		if (!empty($this->id)) {
			$_SESSION['basket'][$this->id] = 1;
		}

		return $this->get();
	}

	/**
	 * Deletes the prepared product from the basket or if no product has set to delete then clear the whole basket.
	 *
	 * @return array   The products in the basket.
	 */
	public function delete()
	{
		if (!empty($this->id)) {
			unset($_SESSION['basket'][$this->id]);
		}
		else {
			unset($_SESSION['basket']);
		}

		return $this->get();
	}

	/**
	 * Sets the order of the basket.
	 *
	 * @param $order
	 */
	public function setOrder($order)
	{
		$this->order = $order;
	}

	/**
	 * Set the product id to work with.
	 *
	 * @param $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * Set the data to work with.
	 *
	 * @param array $data   More information of the product.
	 */
	public function setData($data)
	{
		$this->data = $data;
	}
}