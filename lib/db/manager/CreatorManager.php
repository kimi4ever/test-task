<?php
/**
 * Package lib/db/manager
 * Holds the CreatorManager class
 */

/**
 * Class CreatorManager
 *
 * Help to create the install process.
 */
class CreatorManager extends AbstractManager
{
	/**
	 * Creates the table according to the table file added through the $table parameter.
	 *
	 * @param string $table   Filename of the table class.
	 */
	public function createTable($table)
	{
		$documentor = new DocumentorHelper($table);
		$name       = '';
		$query      = '';
		$primary    = '';

		foreach ($documentor->getConstants() as $constant) {
			if ($constant['variable'] == 'NAME') {
				$name = $constant['name'];
			}
			else {
				if (!empty($constant['comment']['mysql']) && strpos($constant['comment']['mysql'], 'auto_increment') !== false) {
					$primary = $constant['name'];
				}

				$query .= '`' . $constant['name'] . '`' . ((!empty($constant['comment']['mysql']) ? (' ' . strtoupper($constant['comment']['mysql'])) : '')) . ',';
			}
		}

		$this->database->query('CREATE TABLE IF NOT EXISTS ' . $name . '(' . $query . ' PRIMARY KEY (`' . $primary . '`)) CHARACTER SET utf8 COLLATE utf8_unicode_ci;');
	}

	/**
	 * Put the predefined data to the database.
	 */
	public function installPredefinedData()
	{
		$data = array(
			ProductTable::NAME => array(
				array(
					ProductTable::FIELD_ID        => 1001,
					ProductTable::FIELD_TITLE     => 'Dreamweaver CS4',
					ProductTable::FIELD_AUTHOR    => 'Janine Warner',
					ProductTable::FIELD_PUBLISHER => 'PANEM',
					ProductTable::FIELD_PRICE     => 3900
				),
				array(
					ProductTable::FIELD_ID        => 1002,
					ProductTable::FIELD_TITLE     => 'JavaScript kliens oldalon',
					ProductTable::FIELD_AUTHOR    => 'Sikos László',
					ProductTable::FIELD_PUBLISHER => 'BBS-INFO',
					ProductTable::FIELD_PRICE     => 2900
				),
				array(
					ProductTable::FIELD_ID        => 1003,
					ProductTable::FIELD_TITLE     => 'Java',
					ProductTable::FIELD_AUTHOR    => 'Barry Burd',
					ProductTable::FIELD_PUBLISHER => 'PANEM',
					ProductTable::FIELD_PRICE     => 3700
				),
				array(
					ProductTable::FIELD_ID        => 1004,
					ProductTable::FIELD_TITLE     => 'C# 2008',
					ProductTable::FIELD_AUTHOR    => 'Stephen Randy Davis',
					ProductTable::FIELD_PUBLISHER => 'PANEM',
					ProductTable::FIELD_PRICE     => 3700
				),
				array(
					ProductTable::FIELD_ID        => 1005,
					ProductTable::FIELD_TITLE     => 'Az Ajax alapjai',
					ProductTable::FIELD_AUTHOR    => 'Joshua Eichorn',
					ProductTable::FIELD_PUBLISHER => 'PANEM',
					ProductTable::FIELD_PRICE     => 4500
				),
				array(
					ProductTable::FIELD_ID        => 1006,
					ProductTable::FIELD_TITLE     => 'Algoritmusok',
					ProductTable::FIELD_AUTHOR    => 'Ivanyos Gábor, Rónyai Lajos, Szabó Réka',
					ProductTable::FIELD_PUBLISHER => 'TYPOTEX',
					ProductTable::FIELD_PRICE     => 3600
				)
			),
			DiscountTable::NAME => array(
				array(
					DiscountTable::FIELD_ID             => 101,
					DiscountTable::FIELD_TYPE           => '* 0.9',
					DiscountTable::FIELD_PRODUCT_ID     => 1006,
					DiscountTable::FIELD_PUBLISHER_NAME => ''
				),
				array(
					DiscountTable::FIELD_ID             => 102,
					DiscountTable::FIELD_TYPE           => '- 500',
					DiscountTable::FIELD_PRODUCT_ID     => 1002,
					DiscountTable::FIELD_PUBLISHER_NAME => ''
				),
				array(
					DiscountTable::FIELD_ID             => 103,
					DiscountTable::FIELD_TYPE           => '2+1',
					DiscountTable::FIELD_PRODUCT_ID     => 0,
					DiscountTable::FIELD_PUBLISHER_NAME => 'PANEM'
				)
			)
		);

		foreach ($data as $table => $datas) {
			foreach ($datas as $line) {
				$this->database->insert($table, $line);
			}
		}
	}
}