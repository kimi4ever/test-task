<?php

/**
 * Database connection data.
 */
return array(
	'database_type' => 'mysql',
	'database_name' => 'bigfish',
	'server'        => '127.0.0.1',
	'username'      => 'root',
	'password'      => 'root'
);