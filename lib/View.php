<?php
/**
 * Package lib
 * Holds the View class
 */

/**
 * Class View
 *
 * Helps to output the html or json data to the output.
 */
class View
{
	/** HTML output type */
	const OUTPUT_TYPE_HTML = 'html';
	/** JSON output type */
	const OUTPUT_TYPE_JSON = 'json';

	/**
	 * Stores the string to output.
	 *
	 * @var string|array
	 */
	private static $inline_view = '';
	/** @var string Current output type */
	private static $outputType  = self::OUTPUT_TYPE_HTML;

	/**
	 * Sets the output type.
	 *
	 * @param string $type
	 */
	public static function setOutputType($type)
	{
		self::$outputType = $type;
	}

	/**
	 * Sets the output.
	 *
	 * @param string|array $output
	 */
	public static function setOutput($output)
	{
		self::$inline_view = $output;
	}

	/**
	 * Write the content of this class to the output.
	 */
	public static function display()
	{
		switch (self::$outputType) {
			case self::OUTPUT_TYPE_JSON:
				echo json_encode(self::$inline_view);
				break;

			case self::OUTPUT_TYPE_HTML:
				self::header();
				echo self::$inline_view;
				self::footer();
				break;
		}
	}

	/**
	 * HTML header.
	 */
	private static function header()
	{
		?>
		<!DOCTYPE html>
		<html lang="hu">
		<head>
			<meta charset="utf-8">
			<title>Bigfish webshop kosár demo</title>
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<meta name="description" content="bigfish webshop demo próbafeladat">
			<meta name="keywords" content="bigfish, webshop, demo, próbafeladat">

			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
			<link rel="stylesheet" type="text/css" href="/static/css/product.min.css">

			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
			<script src="/static/js/product.js"></script>

		</head>
		<body>
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
				<a class="navbar-brand" href="#">Basket Demo</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav">
						<li class="nav-item">
							<a id="product-menu" class="nav-link" href="#">Products</a>
						</li>
						<li class="nav-item">
							<a id="basket-menu" class="nav-link" href="#">Cart</a>
						</li>
					</ul>
				</div>
			</nav>

			<div id="product-container" class="row">
				<div class="col-12">
					<div class="form-inline">
						<label for="product-sort-select">Sort by&nbsp;&nbsp;&nbsp;</label>
						<select id="product-sort-select" class="form-control">
							<option value="name">name</option>
							<option value="price">price</option>
						</select>
					</div>
				</div>

				<div class="col-sm-9 products"></div>
				<div class="col-sm-3">
					<div class="card">
						<div class="card-body">
							<h4>Cart</h4>
							<div class="small-basket"></div>
						</div>
					</div>
				</div>
			</div>

			<div id="basket-container" class="row d-none">
				<div class="col-12">
					<h4>Cart <a href="#" id="empty-cart" class="btn btn-danger float-right">Empty Cart</a></h4>
				</div>
				<div class="col-12 products"></div>
			</div>
		<?php
	}

	/**
	 * HTML footer.
	 */
	private static function footer()
	{
		?>
		</div>
		</body>
		</html>
		<?php
	}
}