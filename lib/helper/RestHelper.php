<?php
/**
 * Package lib/helper
 * Holds the RestHelper class
 */

/**
 * Class RestHelper
 *
 * Helps to call the manager classes appropriate method using the server's request method variable.
 */
class RestHelper
{
	/**
	 * Request method.
	 *
	 * @var string
	 */
	private $method;
	/**
	 * Instance of a manager class that implements RestManagerInterface
	 *
	 * @var RestManagerInterface
	 */
	private $manager;

	/**
	 * RestHelper constructor.
	 *
	 * Initialize the manager class.
	 */
	public function __construct()
	{
		$this->method  = $_SERVER['REQUEST_METHOD'];
		$className     = ucfirst($_GET['api']) . 'Manager';
		$this->manager = !empty($_GET['api']) ? new $className() : null;

		if (!empty($_GET['order'])) {
			$this->manager->setOrder($_GET['order']);
		}

		if (!empty($_GET['id'])) {
			$this->manager->setId($_GET['id']);
		}

		if (!empty($_GET['data'])) {
			$this->manager->setData($_GET['data']);
		}
	}

	/**
	 * Call the given manager's exact method according to the request method.
	 *
	 * @return mixed
	 */
	protected function output()
	{
		$method = strtolower($this->method);

		return $this->manager->$method();
	}

	/**
	 * Set up View class to show output.
	 */
	public function display()
	{
		View::setOutputType(View::OUTPUT_TYPE_JSON);
		View::setOutput($this->output());
	}
}