<?php
/**
 * Package lib/helper
 * Holds the StringHelper class
 */

/**
 * Class StringHelper
 *
 * Helps to move on with strings.
 */
class StringHelper
{
	/**
	 * Helps to decide that the given string is starts with the given needle.
	 *
	 * @param string $haystack   The string to search in.
	 * @param string $needle     Needle.
	 *
	 * @return bool
	 */
	public static function startsWith($haystack, $needle)
	{
		// search backwards starting from haystack length characters from the end
		return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
	}

	/**
	 * Helps to decide that the given string is ends with the given needle.
	 *
	 * @param string $haystack   The string to search in.
	 * @param string $needle     Needle.
	 *
	 * @return bool
	 */
	public static function endsWith($haystack, $needle)
	{
		// search forward starting from end minus needle length characters
		return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
	}
}