<?php
/**
 * Package lib/helper
 * Holds the DocumentorHelper class
 */

/**
 * Class DocumentorHelper
 *
 * Helps to get the data of constants from a class.
 */
class DocumentorHelper
{
	/**
	 * Stores the constants of the class.
	 *
	 * @var array
	 */
	private $constants;

	/**
	 * DocumentorHelper constructor.
	 * Process the given file. Get the data of the constants from the given file.
	 *
	 * @param $file
	 */
	public function __construct($file)
	{
		$this->constants = array();
		$content         = file_get_contents($file);

		if ($content !== false) {
			$tokens          = token_get_all($content);
			$const           = false;
			$currentComment  = '';
			$currentVariable = '';

			foreach ($tokens as $token) {
				if ($token[0] == T_DOC_COMMENT) {
					$currentComment = $token[1];
				}
				else if ($token[0] == ';') {
					$currentComment  = '';
					$currentVariable = '';
					$const           = false;
				}
				else if ($token[0] == T_CONST) {
					$const = true;
				}
				else if ($token[0] == 308) {
					$currentVariable = $token[1];
				}
				else if ($token[0] == 316 && $const) {
					$comment      = array();
					$describeText = '';

					foreach (explode(PHP_EOL, trim(str_replace('/**', '', str_replace('*/', '', $currentComment)))) as $item) {
						$item = trim($item);

						if (StringHelper::startsWith($item, '*')) {
							$item = trim(ltrim($item, '*'));
						}

						if (StringHelper::startsWith($item, '@')) {
							$comment['describe'][] = $describeText;
							$describeText          = '';
							$tmp                   = explode(' ', $item);
							$key                   = $tmp[0];

							unset($tmp[0]);

							$comment[str_replace('@', '', $key)] = implode(' ', $tmp);
						}
						else {
							$describeText .= $item . ' ';
						}
					}

					$comment['describe'][] = $describeText;
					$comment['describe']   = trim(implode(' ', $comment['describe']));
					$this->constants[]     = array(
						'name'     => trim($token[1], '\''),
						'comment'  => $comment,
						'variable' => $currentVariable
					);
				}
			}
		}
	}

	/**
	 * Returns the data of the class constants.
	 *
	 * @return array
	 */
	public function getConstants()
	{
		return $this->constants;
	}
}