Dependencies:<br>
    - Apache 2.2+<br>
    - PHP 5.6+<br>
    - MySql 5.6+

1. Installation:<br>
    First of all, copy and paste the project files into your webserver's folder, then<br>
    - Create a database in your Mysql distribution.
    - Search file <b>/lib/db/config.php</b> in the project.<br>
    - Change array values for your database connection:
        - database_name: change its value to your database name
        - username: change its value to your mysql username
        - password: change its value to your mysql password
        - server (optional): if your database not running from local, change it to its ip address
     - Open command line, move to the project's folder and then run command **php install.php**. It will create 
     tables in your database. After that you have to delete this file.
     
2. Use:<br>
    Start Apache if you didn't do that early.<br>
    Navigate to localhost in your browser (or if you have set a vhost for it, then navigate to the given url) then start
    to put some books into your cart.